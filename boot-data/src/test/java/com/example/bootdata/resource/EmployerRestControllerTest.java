package com.example.bootdata.resource;


import com.example.bootdata.dao.EmployerJpaRepository;
import com.example.bootdata.dao.CustomerJpaRepository;
import com.example.bootdata.domain.SysRole;
import com.example.bootdata.domain.SysUser;
import com.example.bootdata.domain.dto.EmployerDto;
import com.example.bootdata.domain.dto.AccountRequestDto;
import com.example.bootdata.domain.dto.CustomerDto;
import com.example.bootdata.domain.hr.Account;
import com.example.bootdata.domain.hr.Employer;
import com.example.bootdata.domain.hr.Customer;
import com.example.bootdata.repository.UserRepository;
import com.example.bootdata.resource.CustomerRestController;
import com.example.bootdata.service.*;
import com.example.bootdata.service.dtomapper.*;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.security.test.context.support.WithMockUser;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(RestEmployeeController.class)

public class EmployerRestControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private CustomOAuth2UserService oauthUserService;

    @MockBean
    private CustomerJpaRepository customerJpaRepository;
    @MockBean
    private EmployerJpaRepository employerJpaRepository;
    @MockBean

    private UserRepository userRepository ;
    @MockBean
    private CustomerService customerService;
    @MockBean
    private UserDetailsServiceImpl userDetailsService;


    @MockBean
    private EmployerService employerService;

    @MockBean
    private CustomerRequestDtoMapper requestMapper;
    @MockBean
    private CustomerDtoMapper accountMapper;
    @MockBean
    private EmployerDtoMapper employerMapper;
    @MockBean
    private EmployerRequestDtoMapper requestEmployerMapper;
    @TestConfiguration
    public static class TestConfig {
        @Bean // Тестируем компонентно с меппером
        public UserDtoMapper employeeDtoMapper() {
            return new UserDtoMapper();
        }
    }
    @BeforeEach
    public void setUp()  throws Exception  {
        SysUser user = new SysUser(1L, "1", "1", true, Set.of(new SysRole(1L, "USER", null)));
        when(userDetailsService.findAll()).thenReturn(List.of(user));

    }
    @Test
    @WithMockUser(value = "user1")

    public void getById() throws Exception {
       Employer employer = new Employer();
        employer.setId(1L);
       employer.setName("Samsung");


        EmployerDto employerDto = new EmployerDto();
        employerDto.setId(1L);
        employerDto.setName("Samsung");


        when(employerService.getById(1L)).thenReturn(employer);
        when(employerMapper.convertToDto(employer)).thenReturn(employerDto);

        this.mockMvc.perform(MockMvcRequestBuilders.get("/employees/1").contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is("Samsung")));
    }

    @Test
    @WithMockUser(value = "user1")
    public void findAll() throws Exception {
        Employer employer = new Employer();
        employer.setId(1L);
        employer.setName("Samsung");


        EmployerDto employerDto = new EmployerDto();
        employerDto.setId(1L);
        employerDto.setName("Samsung");
        List<Employer> employers = new ArrayList<>();
        employers .add(employer);

        when(employerService.findAll(0,10)).thenReturn(employers);
        when(employerMapper.convertToDto(employer)).thenReturn(employerDto);
        this.mockMvc.perform(MockMvcRequestBuilders.get("/employees").contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", Matchers.is("Samsung")));
    }
    @Test
    @WithMockUser(value = "user1")
    public void findAllPageable() throws Exception {
        Employer employer = new Employer();
        employer.setId(1L);
        employer.setName("Samsung");


        EmployerDto employerDto = new EmployerDto();
        employerDto.setId(1L);
        employerDto.setName("Samsung");
        List<Employer> employers = new ArrayList<>();
        employers .add(employer);
        int page = 0;
        int size = 10;
        when(employerService.findAll(page,size)).thenReturn(employers);
        when(employerMapper.convertToDto(employer)).thenReturn(employerDto);
        this.mockMvc.perform(MockMvcRequestBuilders.get("/employees/0/10").contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", Matchers.is("Samsung")));
    }

    @Test
    @WithMockUser(value = "user1")
    public void testCreate() throws Exception {


        this.mockMvc.perform(MockMvcRequestBuilders.post("/employees")
                        .contentType("application/json")
                        .content(
                                """
                               {
                                   "id": 10,
                                   "name": "Google",
                                    "location": "Mountain View"
                                 }
                                """
                        ))
                .andExpect(status().isOk())

        ;
    }
    @Test
    @WithMockUser(value = "user1")


    public void testPut() throws Exception {

        this.mockMvc.perform(MockMvcRequestBuilders.put("/employees")
                        .contentType("application/json")
                        .content(
                                """
                               {
                                   "id": 202,
                                   "name": "BurgerKing",
                                   "location": "LA"
                                   
                                 }
                                """
                        ))
                .andExpect(status().isOk())

        ;
    }
    @Test
    @WithMockUser(value = "user1")

    public void testDelete() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.delete("/employees")
                        .contentType("application/json")
                        .content(
                                """
                                
                                   {
                                               "id": 203,
                                               "name": "",
                                               "location": ""
                                             
                                             }
                                   
                                
                                """
                        ))
                .andExpect(status().isOk())

        ;
    }
    @Test
    @WithMockUser(value = "user1")

    public void testDeleteById() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.delete("/employees/205").contentType("application/json"))
                .andExpect(status().isOk())


        ;
    }
}