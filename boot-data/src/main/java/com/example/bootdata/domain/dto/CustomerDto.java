package com.example.bootdata.domain.dto;

import jakarta.validation.constraints.*;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class CustomerDto {

    private Long id;


    private String name;

    private String email;


    private String telNumber;


    private  Integer age;

    protected Date lastModifiedDate;
}
