package com.example.bootdata.domain.hr;

import com.example.bootdata.domain.AbstractEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import java.util.*;


@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@NamedEntityGraph(
        name = "customerFull",
        attributeNodes = {
                @NamedAttributeNode(value = "accounts"),
                @NamedAttributeNode(value = "employers"),
        }
)
@Table(name="customers")
public class Customer extends AbstractEntity {



    private String name;
    private String email;

    private String password;
    @Column (name ="tel_num")
    private String telNumber;
    private  Integer age;



    @OneToMany (cascade = {CascadeType.MERGE,CascadeType.REMOVE },fetch =FetchType.EAGER ,mappedBy = "customer")
    private List<Account> accounts = new ArrayList<>() ;

@LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany(mappedBy = "customers",cascade = {CascadeType.REMOVE})
    private List  <Employer> employers = new ArrayList<>() ;

    public Customer(String name, String email, Integer age) {
        this.name = name;
        this.email = email;
        this.age = age;
    }


    @Override
    public String toString() {
        return "Customer{" +
           //     "id=" + customerId +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", age=" + age +
                '}';
    }




    public String getName() {
        return name;
    }


    public String getEmail() {
        return email;
    }



    public Integer getAge() {
        return age;
    }



    public List<Account> getAccounts() {
        return accounts;
    }


    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }
}

