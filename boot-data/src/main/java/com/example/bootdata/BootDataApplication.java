package com.example.bootdata;



import com.example.bootdata.service.CustomOAuth2UserService;
import com.example.bootdata.service.WebSecurityDbFormConfig;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.EnableTransactionManagement;


//@Slf4j
//@EnableJpaAuditing
@EnableTransactionManagement
@EnableWebSecurity
@SpringBootApplication
@Import(WebSecurityDbFormConfig.class)
public class BootDataApplication implements ApplicationRunner {
    private final CustomOAuth2UserService oauthUserService;





@Autowired
    public BootDataApplication(CustomOAuth2UserService oauthUserService) {
        this.oauthUserService = oauthUserService;


}

    public static void main(String[] args) {
        SpringApplication.run(BootDataApplication.class, args);
    }




    @Override
   // @Transactional
    public void run(ApplicationArguments args) {
        System.out.println("http://localhost:9000/swagger-ui/index.html \n");


    }

    @Bean
    public PasswordEncoder passwordEncoder() {
      // return NoOpPasswordEncoder.getInstance();
        return new BCryptPasswordEncoder();
    }



    @Bean
    public OpenAPI springShopOpenAPI() {
        return new OpenAPI()
                .info(new Info().title("EIS API")
                        .description("Employee Information System sample application")
                        .version("v0.0.1")
                        .license(new License().name("Apache 2.0").url("http://springdoc.org"))
                        .description("SpringShop Wiki Documentation")
                        .contact(new Contact().email("test@test.com").url("http://fullstackcode.dev")))
                ;
    }
}
