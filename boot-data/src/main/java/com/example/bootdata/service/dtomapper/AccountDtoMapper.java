package com.example.bootdata.service.dtomapper;

import com.example.bootdata.domain.dto.AccountDto;

import com.example.bootdata.domain.hr.Account;
import org.springframework.stereotype.Service;


@Service
    public class AccountDtoMapper extends DtoMapperFacade<Account, AccountDto>{
        public AccountDtoMapper (){super(Account.class , AccountDto.class); }
    }


