package com.example.bootdata.model;

import lombok.Data;

@Data
public class SendRequest {
    String type;
    String sender;
    String receiver;
    String text;
}
