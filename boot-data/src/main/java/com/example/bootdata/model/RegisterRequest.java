package com.example.bootdata.model;

import lombok.Data;

@Data
public class RegisterRequest {
    String type;
    String sender;
}
