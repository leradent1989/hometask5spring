package com.example.bootdata.resource;

import com.example.bootdata.config.WebSocketConfig;
import com.example.bootdata.domain.dto.*;
import com.example.bootdata.domain.hr.Account;
import com.example.bootdata.domain.hr.Customer;

import com.example.bootdata.domain.hr.Employer;
import com.example.bootdata.service.AccountService;
import com.example.bootdata.service.CustomerService;

import com.example.bootdata.service.dtomapper.*;
import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;


import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/customers")
@CrossOrigin( origins = {"http://localhost:3000"})
public class CustomerRestController {

    private final CustomerService customerService;
    private final AccountService accountService ;

    private final CustomerDtoMapper dtoMapper;

    private final CustomerRequestDtoMapper requestMapper;

    private  final AccountDtoMapper accountMapper;

    private  final EmployerDtoMapper employerMapper;

    private final AccountRequestDtoMapper requestAccountMapper;

    private static final Logger logger = LoggerFactory.getLogger(WebSocketConfig.class);
    @GetMapping

    public List<CustomerDto> findAll(){
        log.info("Getting all customers (first 10) ");
        return customerService.findAll(0,10).stream().map(dtoMapper::convertToDto).collect(Collectors.toList());

    }
    @GetMapping("/{page}/{size}")
    public List <CustomerDto> findAll(@PathVariable Integer page, @PathVariable Integer size) {
        log.info("Getting  customers by page and size ");
        List<Customer> customers = customerService.findAll(page, size);
        List<CustomerDto> customersDto = customers.stream().map(dtoMapper::convertToDto).collect(Collectors.toList());

        return customersDto ;
    }
    @GetMapping("/{id}/accounts")
    public List <AccountDto> getCustomerAccounts(@PathVariable ("id") Long customerId){

        Customer customer = customerService.getOne(customerId);
        List <Account > customerAccounts = customer.getAccounts();

        if (customer == null){
          logger.error("Customer not found");
        }
        log.info("Getting all accounts of current customer ");
        List < AccountDto> customerAccountsDto = customerAccounts.stream().map(accountMapper ::convertToDto).collect(Collectors.toList());
        return customerAccountsDto ;
    }
    @GetMapping("/{id}/employers")
    public List <EmployerDto> getCustomerCompanies(@PathVariable ("id") Long customerId){
        Customer customer = customerService.getOne(customerId);
        List <Employer > customerCompanies = customer.getEmployers();

        if (customer == null){


           log.error("Customer not found");
            throw  new EntityNotFoundException();
        }
        log.info("Getting all employees of current customer ");
        List <EmployerDto> customerEmployerDto = customerCompanies.stream().map(employerMapper ::convertToDto).collect(Collectors.toList());
 return customerEmployerDto ;
    }
    @GetMapping("/{id}")
    public CustomerDto getById(@PathVariable ("id") Long customerId){
        log.info("Getting  customer by id ");
        Customer customer = customerService.getOne(customerId);
        if (customer  == null){
        logger.error("Customer not found");

        }

        return dtoMapper.convertToDto(customer) ;
    }

    @PostMapping
    public void create( @Valid  @RequestBody CustomerRequestDto customer ){
        log.info("Trying to create new customer ");
        customerService.save(requestMapper.convertToEntity(customer) );

    }

    @PutMapping
    public void update(@Valid  @RequestBody CustomerRequestDto customerRequestDto ){

            log.info("Trying to update customer ");
  customerService.update(requestMapper.convertToEntity(customerRequestDto));



    }

    @DeleteMapping("/{id}")
    public void  deleteById  (@PathVariable ("id") Long customerId){

            log.info("Trying to delete customer ");
            customerService.deleteById(customerId);




    }
      @DeleteMapping

    public void   deleteCustomer (  @RequestBody CustomerRequestDto customer) throws IllegalArgumentException{

    log.info("Trying to delete customer ");
    if(customerService.getOne(customer.getId()) == null){
        throw  new IllegalArgumentException("Customer not found");
    }


   customerService.delete(requestMapper.convertToEntity(customer));


    }



    @PutMapping("/account/{id}")

    public ResponseEntity <?>  addAccount(@PathVariable Long id,@RequestBody AccountRequestDto accountRequestDto ){


        Account account = requestAccountMapper.convertToEntity(accountRequestDto);
        log.info("Trying to add new account to customer ");

        if(customerService.addAccount(id,account) == false){
            logger.error("Incorrect account number");


        }

        return ResponseEntity.ok().build();

    }
    @DeleteMapping("/account/{id}")

    public void deleteAccount(@PathVariable  Long id, @RequestBody AccountRequestDto accountRequestDto ){
        Account account = requestAccountMapper.convertToEntity(accountRequestDto);

        log.info("Trying to delete account of current customer ");

       if(accountService.getOne(id) == null){
           log.error("Account does not exist");

        }
        Customer customer = customerService.getOne(id);

        List <Account > customerAccounts = customer.getAccounts();


        if(!customerAccounts.contains(accountService.getOne(account.getId()))){
            logger.error("This account isn't in the list");

        }
        Account account1 =accountService.getOne(account.getId());
        account1.setCustomer(null);
        customerAccounts.remove(accountService.getOne(account.getId()));
        customer.setAccounts(customerAccounts);

        customerService.update(customer);
        accountService.update(account1);




    }
    @ExceptionHandler(EntityNotFoundException.class)

    public ResponseEntity<?> handleEntityNotFoundException(
            EntityNotFoundException ex, WebRequest request) {

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("message", ex.getLocalizedMessage());

        return new ResponseEntity<>(body, HttpStatus.NOT_FOUND);

    }


}