package com.example.bootdata.resource;

import com.example.bootdata.domain.dto.CustomerDto;
import com.example.bootdata.domain.dto.EmployerDto;
import com.example.bootdata.domain.dto.EmployerRequestDto;
import com.example.bootdata.domain.hr.Customer;
import com.example.bootdata.domain.hr.Employer;


import com.example.bootdata.service.EmployerService;
import com.example.bootdata.service.dtomapper.EmployerDtoMapper;
import com.example.bootdata.service.dtomapper.EmployerRequestDtoMapper;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/employees")
@CrossOrigin( origins = {"http://localhost:3000"})
public class RestEmployeeController {
    private final EmployerService employeeService;

    private final EmployerDtoMapper dtoMapper;
    private final EmployerRequestDtoMapper requestMapper;

    @GetMapping
    public List<EmployerDto > getAll() {
        log.info("Getting all employers(first 10)");
        return employeeService.findAll(0,10).stream().map(dtoMapper::convertToDto).collect(Collectors.toList());
    }
    @GetMapping("/{page}/{size}")

    public List <EmployerDto> findAll(@PathVariable Integer page, @PathVariable Integer size) {
        log.info("Getting employers by page and size");
        List<Employer> employers =employeeService.findAll(page, size);
        List<EmployerDto> employersDto = employers.stream().map(dtoMapper::convertToDto).collect(Collectors.toList());

        return employersDto;
    }
    @GetMapping("/{id}")
    public EmployerDto getById(@PathVariable("id")  Long  userId) {
        log.info("Get employer by id");
       Employer  employee = employeeService.getById(userId );

        if (employee   == null){
      log.error("Employer not found");
        }
        return dtoMapper.convertToDto(employee ) ;
    }
    @GetMapping("/customers/{id}")
    public List <Customer> getCustomers(@PathVariable("id")  Long  userId) {
        log.info("Get customers working in employer");
        Employer  employee = employeeService.getById(userId );

        if (employee   == null){
          log.error("Employer not found");
        }
        return employee.getCustomers() ;
    }
    @PostMapping
    public void create( @Valid @RequestBody EmployerRequestDto employee ){
        log.info("Trying to create new employer and add to database");
        employeeService.create(requestMapper.convertToEntity(employee) );
    }
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id")Long userId) {
   log.info("Deleting employer by id");
          employeeService.deleteById(userId);

    }
    @DeleteMapping
    public void deleteEmployee(@RequestBody EmployerRequestDto  company) {
        log.info("Deleting employer");

            employeeService.delete(requestMapper.convertToEntity(company));

    }
    @PutMapping
    public void update( @Valid @RequestBody EmployerRequestDto employee) {

  log.info("Trying to update employer");

            employeeService.update(requestMapper.convertToEntity(employee));


    }


}
