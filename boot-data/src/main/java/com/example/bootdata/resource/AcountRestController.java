package com.example.bootdata.resource;


import com.example.bootdata.config.WebSocketConfig;
import com.example.bootdata.domain.dto.AccountDto;
import com.example.bootdata.domain.dto.AccountRequestDto;
import com.example.bootdata.domain.dto.CustomerDto;
import com.example.bootdata.domain.hr.Account;


import com.example.bootdata.service.AccountService;


import com.example.bootdata.service.dtomapper.AccountDtoMapper;
import com.example.bootdata.service.dtomapper.AccountRequestDtoMapper;
import com.example.bootdata.service.dtomapper.CustomerDtoMapper;

import com.example.bootdata.websocket.SocketHandler;
import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;
@Slf4j
@RestController
@Setter

@RequiredArgsConstructor
//@Component

@RequestMapping("/accounts")
@CrossOrigin(origins = {"http://localhost:3000"})
public class AcountRestController {

    private  final   AccountService accountService;


    private   SocketHandler socketHandler;

    private final    AccountDtoMapper dtoMapper;
    private  final   CustomerDtoMapper customerMapper;

    private  final   AccountRequestDtoMapper requestDtoMapper;
    private static final Logger logger = LoggerFactory.getLogger(WebSocketConfig.class);


    @GetMapping

    public List<AccountDto> findAll(){
        log.info("Getting all accounts(first 10)");
        return accountService.findAll().stream()
                .map(dtoMapper::convertToDto)
                .collect(Collectors.toList());

    }
    @GetMapping("/{page}/{size}")
    public List<AccountDto > findAll(@PathVariable Integer page, @PathVariable Integer size) {
        log.info("Getting accounts by page and size");
        List<Account> accounts = accountService.findAll(page, size);
        List<AccountDto> accountsDto = accounts .stream().map(dtoMapper::convertToDto).collect(Collectors.toList());

        return accountsDto;
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable ("id") Long studentId){
   Account account = accountService.getOne(studentId);
        log.info("Getting account by account id");
        if (account  == null) {


            logger.error("Account not found");

            throw new EntityNotFoundException();
        }

        return ResponseEntity.ok(dtoMapper.convertToDto(account)) ;
    }
    @GetMapping("/{id}/customers")
    public CustomerDto getCustomer(@PathVariable ("id") Long studentId){

        Account account = accountService.getOne(studentId);

        log.info("Getting customer of current account ");
        if (account  == null){
            logger.error("Account not found");

        }
        return customerMapper.convertToDto(account.getCustomer()) ;
    }


    @PostMapping
    public ResponseEntity <?> create( @Valid @RequestBody AccountRequestDto account ){
        log.info("Creating new account ");
        accountService.save(requestDtoMapper.convertToEntity( account));
        return ResponseEntity.ok().build();
    }

    @PutMapping
    public ResponseEntity <?> update(@Valid   @RequestBody AccountRequestDto accountRequestDto ){

            log.info("Updating account ");

            accountService.update(requestDtoMapper.convertToEntity(accountRequestDto)) ;

return ResponseEntity.ok().build();

    }
    @DeleteMapping

    public ResponseEntity <?>  deleteAccount(@RequestBody AccountRequestDto  account){


            log.info("Deleting account ");

            accountService.delete(requestDtoMapper.convertToEntity(account));
return ResponseEntity.ok().build();




    }
    @DeleteMapping("/{id}")
    public ResponseEntity <?> delete(@PathVariable ("id") Long studentId){

            log.info("Deleting account ");
            accountService.deleteById(studentId);

 return ResponseEntity.ok().build();



    }

    @PutMapping("/balance")
    public ResponseEntity <?>   addMoney (@RequestParam UUID number, @RequestParam Double  amount ){

        log.info("Updating account balance ");

        if(accountService.addMoney(number,amount) == false){
            logger.error("Can not update account");

        }


        //   try{

       //    socketHandler.handleTextMessage(socketHandler.getSessions().get("user"), new TextMessage("ok"));
       //   }catch (InterruptedException e){}catch (
       //    IOException e){}



        logger.info("ok");

 return ResponseEntity.ok().build();


    }

    @PutMapping("/balance/{id}/{amount}")
    public ResponseEntity <?>   replenishAccount(@PathVariable ("id") Long accountId,@PathVariable ("amount") Double amount){


        Account editedAccount = accountService.getOne(accountId);

        Double accountBalance =  accountService.getOne(accountId).getBalance();

        Double accountNewBalance = accountBalance + amount ;

        editedAccount.setBalance(accountNewBalance);

        accountService.update( editedAccount);
     //  Map <String,String> messages =  socketHandler.getMessagesToUsers();
       // messages.put("user","Account is updated");
      //  socketHandler.setMessagesToUsers(messages);
//logger.warn(socketHandler.getMessagesToUsers().toString() +"!!!!!!");
return ResponseEntity.ok().build();


    }


    @DeleteMapping("/balance")
    public void   takeMoney(  @RequestParam  UUID  number, @RequestParam Double  amount ){


        if(accountService.findAll().stream().filter(el->el.getNumber().equals(number.toString())).findAny().isPresent()){
            if(accountService.findAll().stream().filter(el->el.getNumber().equals(number.toString())).findAny().get().getBalance() -amount <0){
                log.error("There is not enough money on your account");
            }
        }

        log.info("Updating account balance ");
if(accountService.takeMoney(number,amount) == false){

    logger.error("Can not update account");
  }


}


    @PutMapping("/balance/transaction")

    public void    transactionMoney(  @RequestParam  UUID  numberAccountFrom, @RequestParam  UUID  numberAccountTo, @RequestParam Double  amount ){
        Optional  <Account> accountFromOptional = accountService.findAll().stream().filter(el->el.getNumber().equals(numberAccountFrom.toString())).findAny();
        if(accountFromOptional.isEmpty()){
            log.error("Account not found");
        }
        Optional  <Account> accountToOptional = accountService.findAll().stream().filter(el->el.getNumber().equals(numberAccountTo.toString())).findAny();
        if(accountToOptional.isEmpty()){
            log.error("Account not found");
        }
        if(accountFromOptional.get().getBalance() -amount <0){
            log.error("Not enough money on account");

        }

        log.info("Trying to perform transaction between accounts ");
        takeMoney(numberAccountFrom ,amount);
        addMoney(numberAccountTo,amount) ;



    }


}
