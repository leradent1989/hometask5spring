package com.example.bootdata.websocket;



import lombok.Getter;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;


import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


@Component
@Getter
@Setter
@Slf4j

public class SocketHandler extends TextWebSocketHandler {

    private     Map<String, WebSocketSession> sessions ;
    private    Map<String, String >  messagesToUsers ;
@Autowired
    public SocketHandler( ) {
        this.sessions = new ConcurrentHashMap<>();
        this.messagesToUsers= new ConcurrentHashMap<>();
    }


    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message) throws InterruptedException, IOException {

        log.info( messagesToUsers.toString());

        if(message.getPayload().equals("ok")){
        session.sendMessage(new TextMessage("Account updated"));

        }


    }

        @Override
        public void afterConnectionEstablished(WebSocketSession session) throws Exception {

            InetSocketAddress clientAddress = session.getRemoteAddress();
            HttpHeaders handshakeHeaders = session.getHandshakeHeaders();

            log.info("Accepted connection from: {}:{}", clientAddress.getHostString(), clientAddress.getPort());

             log.info(this.getSessions().toString());
             sessions.put("user",session);
             this.setSessions(sessions);

             log.info(this.getSessions().get("user").toString());
        }

            @Override
            public void afterConnectionClosed (WebSocketSession session, CloseStatus status) throws Exception {

                log.info("Connection closed by {}:{}", session.getRemoteAddress().getHostString(), session.getRemoteAddress().getPort());
                super.afterConnectionClosed(session, status);
                sessions.clear();
            }



        }


