import React,{useRef} from "react";
import axios from "axios";



const AddMoneyForm = (props) => {

    const ws = props.ws;

    const idRef = useRef(null);
    const amountRef = useRef(null);
    const handleSubmit = (e) => {
        e.preventDefault();


        fetch(`http://localhost:9000/accounts/balance/${idRef.current.value}/${amountRef.current.value}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            }

        }).then(res => {
       if(res.status == 200){console.log('OK')
               ws.send("ok")}

        })

    }

    return (
        <form onSubmit={(e) => handleSubmit(e)}>


            <input
                type="text"
                name="id"
                placeholder="AccountId"
                ref={idRef}
            /><br/>

            <input
                type="text"
                name="amount"
                placeholder="Amount"
                ref={amountRef}
            /><br/>


            <button type="submit">Add money</button>
        </form>
    )
}

export default AddMoneyForm;