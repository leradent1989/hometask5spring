import React,{useRef} from "react";




const AccountForm = () => {

    const idRef = useRef(null);
    const numberRef = useRef(null);
    const currencyRef = useRef(null);


    const handleSubmit = (e) => {
        e.preventDefault();

        const body = {
            id: idRef.current.value,
            number: numberRef.current.value,
            currency: currencyRef.current.value,
        }

        console.log(body);
        fetch(`http://localhost:9000/customers/account/${idRef.current.value}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({id:null, number:body.number,currency:body.currency,balance:0 })
        })

    }





    return (
        <form onSubmit={(e) => handleSubmit(e)}>


            <input
                type="text"
                name="id"
                placeholder="Id"
                ref={idRef}
            /><br/>

            <input
                type="text"
                name="number"
                placeholder="Number"
                ref={numberRef}
            /><br/>

            <input
                type="text"
                name="currency"
                placeholder="Currency"
                ref={currencyRef}
            /><br/>

            <button type="submit">Add new account</button>
        </form>
    )
}

export default AccountForm;